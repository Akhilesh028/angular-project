(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _stage_selection_stage_selection_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stage-selection/stage-selection.component */ "./src/app/stage-selection/stage-selection.component.ts");
/* harmony import */ var _service_selection_service_selection_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service-selection/service-selection.component */ "./src/app/service-selection/service-selection.component.ts");
/* harmony import */ var _num_verify_num_verify_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./num-verify/num-verify.component */ "./src/app/num-verify/num-verify.component.ts");
/* harmony import */ var _gender_selection_gender_selection_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gender-selection/gender-selection.component */ "./src/app/gender-selection/gender-selection.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./confirmation/confirmation.component */ "./src/app/confirmation/confirmation.component.ts");
/* harmony import */ var _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./thank-you/thank-you.component */ "./src/app/thank-you/thank-you.component.ts");
/* harmony import */ var _stages_selection_stages_selection_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./stages-selection/stages-selection.component */ "./src/app/stages-selection/stages-selection.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    // { path: '', redirectTo: '/gender-selection', pathMatch: 'full' },
    { path: 'stage-selection', component: _stage_selection_stage_selection_component__WEBPACK_IMPORTED_MODULE_2__["StageSelectionComponent"] },
    { path: 'service-selection', component: _service_selection_service_selection_component__WEBPACK_IMPORTED_MODULE_3__["ServiceSelectionComponent"] },
    { path: 'stages-selection', component: _stages_selection_stages_selection_component__WEBPACK_IMPORTED_MODULE_9__["StagesSelectionComponent"] },
    { path: 'num-verify', component: _num_verify_num_verify_component__WEBPACK_IMPORTED_MODULE_4__["NumVerifyComponent"] },
    { path: 'gender-selection', component: _gender_selection_gender_selection_component__WEBPACK_IMPORTED_MODULE_5__["GenderSelectionComponent"] },
    { path: 'confirmation', component: _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_7__["ConfirmationComponent"] },
    { path: 'thankyou', component: _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_8__["ThankYouComponent"] },
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"loader_form\">\n  <div data-loader=\"circle-side-2\"></div>\n</div>\n\n\n\n\n<main>\n  <router-outlet></router-outlet>\n  <!-- /Container -->\n</main>\n\n\n<div>\n  <app-header></app-header>\n  \n  <main>\n\n    <!-- <app-gender-selection></app-gender-selection> -->\n\n    <!-- <app-stage-selection></app-stage-selection>\n\n    <app-service-selection></app-service-selection>\n\n    <app-hair-service></app-hair-service>\n\n    <app-num-verify></app-num-verify> -->\n\n    \n\n    <router-outlet></router-outlet>\n\n    <app-footer></app-footer>\n\n  </main>\n\n</div>\n\n\n<div class=\"cd-overlay-nav\">\n  <span></span>\n</div>\n<!-- /cd-overlay-nav -->\n\n\n\n<div class=\"cd-overlay-content\">\n  <span></span>\n</div>\n<!-- /cd-overlay-content -->\n\n\n\n<a href=\"#0\" class=\"cd-nav-trigger\">Menu<span class=\"cd-icon\"></span></a>\n<!-- /cd-nav-trigger -->\n\n\n\n<!-- Modal terms -->\n<div class=\"modal fade\" id=\"terms-txt\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"termsLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"termsLabel\">Terms and conditions</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus\n          tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt\n          sensibus.</p>\n        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id.\n          No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus. Lorem ipsum\n          dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores\n          id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>\n        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id.\n          No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'auto-project';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _stage_selection_stage_selection_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./stage-selection/stage-selection.component */ "./src/app/stage-selection/stage-selection.component.ts");
/* harmony import */ var _service_selection_service_selection_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./service-selection/service-selection.component */ "./src/app/service-selection/service-selection.component.ts");
/* harmony import */ var _num_verify_num_verify_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./num-verify/num-verify.component */ "./src/app/num-verify/num-verify.component.ts");
/* harmony import */ var _gender_selection_gender_selection_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./gender-selection/gender-selection.component */ "./src/app/gender-selection/gender-selection.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./confirmation/confirmation.component */ "./src/app/confirmation/confirmation.component.ts");
/* harmony import */ var _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./thank-you/thank-you.component */ "./src/app/thank-you/thank-you.component.ts");
/* harmony import */ var _stages_selection_stages_selection_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./stages-selection/stages-selection.component */ "./src/app/stages-selection/stages-selection.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_6__["FooterComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _stage_selection_stage_selection_component__WEBPACK_IMPORTED_MODULE_8__["StageSelectionComponent"],
                _service_selection_service_selection_component__WEBPACK_IMPORTED_MODULE_9__["ServiceSelectionComponent"],
                _num_verify_num_verify_component__WEBPACK_IMPORTED_MODULE_10__["NumVerifyComponent"],
                _gender_selection_gender_selection_component__WEBPACK_IMPORTED_MODULE_11__["GenderSelectionComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_12__["HomeComponent"],
                _confirmation_confirmation_component__WEBPACK_IMPORTED_MODULE_13__["ConfirmationComponent"],
                _thank_you_thank_you_component__WEBPACK_IMPORTED_MODULE_14__["ThankYouComponent"],
                _stages_selection_stages_selection_component__WEBPACK_IMPORTED_MODULE_15__["StagesSelectionComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/confirmation/confirmation.component.css":
/*!*********************************************************!*\
  !*** ./src/app/confirmation/confirmation.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "style {\n    text-align: center\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZmlybWF0aW9uL2NvbmZpcm1hdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0NBQ3JCIiwiZmlsZSI6InNyYy9hcHAvY29uZmlybWF0aW9uL2NvbmZpcm1hdGlvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3R5bGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/confirmation/confirmation.component.html":
/*!**********************************************************!*\
  !*** ./src/app/confirmation/confirmation.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n  <div>\n    <form method=\"POST\">\n      \n      <div id=\"middle-wizard\">\n\n        <!-- First branch What Type of Project ============================== -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>Please confirm your selection</h3>\n          </div>\n\n          \n          <div class=\"col-lg-8\">\n            <div class=\"form-group\">\n              <label for=\"gender\">Gender Selection</label>\n              <input name=\"dataMale\" type=\"text\" class=\"form-control\" value={{dataMale}}> \n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"stage\">Stage Selection</label>\n              <input name=\"stage\" type=\"text\" class=\"form-control\" value={{dataStage}}>\n            </div>\n\n            <div class=\"form-group\">\n              <label for=\"service\">Service Selection</label>\n              <input name=\"service\" type=\"text\" class=\"form-control\" value={{dataService}}>\n            </div>\n          </div>\n          \n\n          <div>\n            <p style=\"text-align:center\">If any changes to be made then please click on back button</p>\n          </div>\n\n\n          <div id=\"bottom-wizard\">\n            <button type=\"button\" name=\"backward\" routerLink=\"/service-selection\" class=\"backward\">Backward </button>\n            \n            <button type=\"button\" name=\"forward\" (click)=\"onSubmit()\"  class=\"forward\">Confirm</button>\n          </div>\n          \n\n\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/confirmation/confirmation.component.ts":
/*!********************************************************!*\
  !*** ./src/app/confirmation/confirmation.component.ts ***!
  \********************************************************/
/*! exports provided: ConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationComponent", function() { return ConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _confirmation_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirmation.service */ "./src/app/confirmation/confirmation.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConfirmationComponent = /** @class */ (function () {
    function ConfirmationComponent(catService, router) {
        this.catService = catService;
        this.router = router;
    }
    ConfirmationComponent.prototype.ngOnInit = function () {
        this.getMaleData();
        this.getStageData();
        this.getService();
    };
    ConfirmationComponent.prototype.onSubmit = function () {
        var _this = this;
        var leadObj = {
            gender: this.dataMale,
            stage: this.dataStage,
            service: this.dataService,
            userinformat: localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].userInfo),
            userId: localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].getIdDb)
        };
        return this.catService.categorys(leadObj).subscribe(function (resp) {
            console.log(resp);
            _this.router.navigate(['/thankyou']);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Gender);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Stage);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Service);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].user);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].getIdDb);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].userInfo);
            localStorage.removeItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Stages);
        });
    };
    ConfirmationComponent.prototype.getMaleData = function () {
        this.dataMale = localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Gender);
        console.log(this.dataMale);
    };
    ConfirmationComponent.prototype.getStageData = function () {
        this.dataStage = localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Stage);
        console.log(this.dataStage);
    };
    ConfirmationComponent.prototype.getService = function () {
        this.dataService = localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].Service);
        console.log(this.dataService);
    };
    ConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-confirmation',
            template: __webpack_require__(/*! ./confirmation.component.html */ "./src/app/confirmation/confirmation.component.html"),
            styles: [__webpack_require__(/*! ./confirmation.component.css */ "./src/app/confirmation/confirmation.component.css")],
            providers: [_confirmation_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationService"]]
        }),
        __metadata("design:paramtypes", [_confirmation_service__WEBPACK_IMPORTED_MODULE_2__["ConfirmationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ConfirmationComponent);
    return ConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/confirmation/confirmation.service.ts":
/*!******************************************************!*\
  !*** ./src/app/confirmation/confirmation.service.ts ***!
  \******************************************************/
/*! exports provided: ConfirmationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationService", function() { return ConfirmationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var ConfirmationService = /** @class */ (function () {
    function ConfirmationService(httpClient) {
        this.httpClient = httpClient;
        this.lead = 'http://52.66.249.189:3011/leads';
    }
    ConfirmationService.prototype.categorys = function (leadObj) {
        var objects = leadObj;
        return this.httpClient.post(this.lead, objects, httpOptions);
    };
    ConfirmationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ConfirmationService);
    return ConfirmationService;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer>\n  <div class=\"container clearfix\">\n    <ul>\n      <li><a href=\"index.html\" class=\"animated_link active\">Multiple Branch</a></li>\n      <li><a href=\"index-2.html\" class=\"animated_link\">Single Branch</a></li>\n      <li><a href=\"index-3.html\" class=\"animated_link\">Without Branch</a></li>\n      <li><a href=\"index-4.html\" class=\"animated_link\">Images Version</a></li>\n      <li><a href=\"https://themeforest.net/item/steps-multipurpose-working-wizard-with-branches/21134396?ref=ansonika\" class=\"animated_link\" target=\"_parent\"><strong>Purchase this Template</strong></a></li>\n    </ul>\n    <p>© 2017 Steps</p>\n  </div>\n</footer>"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/gender-selection/gender-selection.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/gender-selection/gender-selection.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dlbmRlci1zZWxlY3Rpb24vZ2VuZGVyLXNlbGVjdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/gender-selection/gender-selection.component.html":
/*!******************************************************************!*\
  !*** ./src/app/gender-selection/gender-selection.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div id=\"wizard_container\">\n    <form name=\"example-1\" id=\"wrapped\" method=\"POST\" ngform>\n      <input id=\"website\" name=\"website\" type=\"text\">\n      <div id=\"middle-wizard\">\n\n        <!-- This is the hearder division where title is present -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>Please select one of the following</h3>\n          </div>\n\n          <!-- This is the option selection for the gender -->\n          <div class=\"row\">\n            <div class=\"col-lg-6\">\n              <div class=\"item\">\n                <input id=\"male\" name=\"male\" type=\"radio\" value=\"male\" (click)=\"getMale()\" class=\"required\">\n                <label for=\"male\"><img src=\"./assets/img/svg/man-2.svg\" alt=\"\" style=\"width:120px; height:120px;\"><strong>Male</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-lg-6\">\n                <div class=\"item\">\n                  <input id=\"female\" name=\"female\" type=\"radio\" value=\"female\" (click)=\"getFemale()\" class=\"required\"> \n                  <label for=\"female\"><img src=\"./assets/img/svg/girl-1.svg\" alt=\"\" style=\"width:120px; height:120px;\"><strong>Female</strong></label>\n                </div>\n              </div>\n\n\n\n            <div class=\"col-lg-12\">\n              <div id=\"bottom-wizard\">\n\n\n                <!-- <button type=\"button\" name=\"forward\" routerLink=\"/container2\" class=\"forward\">Forward</button> -->\n\n                <button type=\"button\" name=\"backward\" routerLink=\"/\" class=\"backward\">Backward </button>\n\n              </div>\n            </div>\n\n\n\n\n\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/gender-selection/gender-selection.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/gender-selection/gender-selection.component.ts ***!
  \****************************************************************/
/*! exports provided: GenderSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GenderSelectionComponent", function() { return GenderSelectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GenderSelectionComponent = /** @class */ (function () {
    function GenderSelectionComponent(router) {
        this.router = router;
    }
    GenderSelectionComponent.prototype.getMale = function () {
        this.a = "Male";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Gender, this.a);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Gender));
        this.router.navigate(["/stage-selection"]);
    };
    GenderSelectionComponent.prototype.getFemale = function () {
        this.a = "Female";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Gender, this.a);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Gender));
        this.router.navigate(["/stages-selection"]);
    };
    GenderSelectionComponent.prototype.ngOnInit = function () { };
    GenderSelectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-gender-selection',
            template: __webpack_require__(/*! ./gender-selection.component.html */ "./src/app/gender-selection/gender-selection.component.html"),
            styles: [__webpack_require__(/*! ./gender-selection.component.css */ "./src/app/gender-selection/gender-selection.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], GenderSelectionComponent);
    return GenderSelectionComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>   \n  <div class=\"container-fluid\">\n      <div class=\"row\">\n              <div class=\"col-3\">\n                  <div id=\"logo_home\">\n                      <h1><a href=\"index.html\">Welcome to the registration page</a></h1>\n                  </div>\n              </div>\n              <div class=\"col-9\">\n                  <div id=\"social\">\n                      <ul>\n                          <li><a href=\"#0\"><i class=\"icon-facebook\"></i></a></li>\n                          <li><a href=\"#0\"><i class=\"icon-twitter\"></i></a></li>\n                          <li><a href=\"#0\"><i class=\"icon-google\"></i></a></li>\n                          <li><a href=\"#0\"><i class=\"icon-linkedin\"></i></a></li>\n                      </ul>\n                  </div>\n                  <!-- /social -->\n                  <nav>\n                      <ul class=\"cd-primary-nav\">\n                          <li><a href=\"about.html\" class=\"animated_link\">About</a></li>\n                          <li><a href=\"contacts.html\" class=\"animated_link\">Contacts</a></li>\n                          <li><a href=\"icon-pack-1.html\" class=\"animated_link\">Icon Pack One</a></li>\n                          <li><a href=\"icon-pack-2.html\" class=\"animated_link\">Icon Pack Two</a></li>\n                          <li><a href=\"icon-pack-3.html\" class=\"animated_link\">Icon Pack Three</a></li>\n                      </ul>\n                  </nav>\n              </div>\n          </div>\n  </div>\n  <!-- /container -->\n</header>\n<!-- /Header -->\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html {\n    background-color: lightgrey;\n    background-image: linear-gradient(to bottom right, #180cac);\n    color: #fff;\n    font-family: \"Quicksand\", sans-serif;\n    font-size: 16px;\n    -moz-osx-font-smoothing: grayscale;\n    -webkit-font-smoothing: antialiased;\n    line-height: 1.5;\n    min-height: 100vh;\n    min-width: 300px;\n    overflow-x: hidden;\n    text-shadow: 0 3px 5px rgba(0, 0, 0, 0.1);\n}\n\na {\n    color: currentColor; cursor: pointer; text-decoration: none;\n}\n\n/* 2. Shared */\n\n.wallpaper,\n.picture-shadow,\n.picture-image {\n  display: block;\n  height: 100%;\n  left: 0;\n  top: 0;\n  width: 100%;\n}\n\n.job,\n.button {\n  font-family: \"Montserrat\", \"Quicksand\", sans-serif;\n  letter-spacing: 0.3em;\n  text-transform: uppercase;\n}\n\n.button,\n.social a {\n  -webkit-transform-origin: center;\n          transform-origin: center;\n  transition-duration: 100ms;\n}\n\n/* 3. Specific */\n\n.wallpaper {\n    background-image: url(\"/images/mountains.jpg\");\n    background-position: center;\n    background-size: cover;\n    opacity: 0.2;\n    position: fixed;\n}\n\n.content {\n    display: flex;\n    position: relative;\n    min-height: 100vh;\n}\n\n.side {\n    max-height: 20rem;\n    max-width: 20rem;\n}\n\n.about {\n    max-width: 26rem;\n}\n\n.picture {\n    padding-top: 100%;\n    position: relative;\n    width: 100%;\n}\n\n.picture-shadow {\n    border-radius: 50%;\n    background-image: radial-gradient(#000 0%, rgba(0, 0, 0, 0) 70%);\n    position: absolute;\n    top: 10%;\n}\n\n.picture-image {\n    border-radius: 10%;\n    position: absolute;\n}\n\n.name {\n    font-size: 2.25rem;\n    line-height: 1.125;\n    margin-bottom: 0.5rem;\n}\n\n.job {\n    color: #ffe479;\n    font-size: 0.75rem;\n}\n\n.hr {\n    background-color: #ff470f;\n    border: none;\n    content: \"\";\n    height: 1px;\n    margin-bottom: 1.5rem;\n    margin-top: 1.5rem;\n    -webkit-transform-origin: center left;\n            transform-origin: center left;\n    width: 4rem;\n}\n\n.description {\n    font-size: 1.5rem;\n}\n\n.contact {\n    display: inline-block;\n    margin-top: 1.5rem;\n    vertical-align: top;\n}\n\n.button {\n    background-color: #fff;\n    border-radius: 290486px;\n    box-shadow: 0 1rem 2rem rgba(0, 0, 0, 0.2);\n    color:red;\n    display: inline-block;\n    font-size: 0.875rem;\n    line-height: 1;\n    padding: 1.25em 2em;\n    text-shadow: none;\n    transition-property: box-shadow, -webkit-transform;\n    transition-property: box-shadow, transform;\n    transition-property: box-shadow, transform, -webkit-transform;\n    -webkit-user-select: none;\n       -moz-user-select: none;\n        -ms-user-select: none;\n            user-select: none;\n    vertical-align: top;\n    white-space: nowrap;\n    will-change: box-shadow, transform;\n}\n\n.button:hover {\n    box-shadow: 0 1.5rem 3rem rgba(0, 0, 0, 0.2);\n    -webkit-transform: scale(1.02) translateY(-4px);\n            transform: scale(1.02) translateY(-4px);\n}\n\n.button:active {\n    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.3);\n    -webkit-transform: scale(0.98) translateY(-2px);\n            transform: scale(0.98) translateY(-2px);\n}\n\n.social {\n    display: flex;\n    margin-top: 1.5rem;\n}\n\n.social li {\n    height: 2rem;\n    margin-right: 0.5rem;\n    text-align: center;\n    width: 2rem;\n}\n\n.social a {\n    align-items: center;\n    display: flex;\n    font-size: 1.5rem;\n    height: 2rem;\n    justify-content: center;\n    opacity: 0.5;\n    transition-property: opacity, -webkit-transform;\n    transition-property: opacity, transform;\n    transition-property: opacity, transform, -webkit-transform;\n    width: 2rem;\n    will-change: opacity, transform;\n}\n\n.social a:hover {\n    opacity: 1;\n    -webkit-transform: scale(1.25);\n            transform: scale(1.25);\n}\n\n.social a:active {\n    opacity: 1;\n    -webkit-transform: scale(1.1);\n            transform: scale(1.1);\n}\n\n/* 4. Responsiveness */\n\n@media screen and (max-width: 799px) {\n    .content {\n      flex-direction: column;\n      justify-content: center;\n      align-items: center;\n      padding: 5rem 3rem;\n    }\n    .side {\n      margin-bottom: 3rem;\n      width: 100%;\n    } \n}\n\n@media screen and (min-width: 800px) {\n    .content {\n      align-items: center;\n      justify-content: space-around;\n      justify-content: space-evenly;\n      padding: 4rem;\n    }\n    .side {\n      flex-grow: 0;\n      flex-shrink: 0;\n      height: 20rem;\n      margin-left: 4rem;\n      order: 2;\n      width: 20rem;\n    }\n    .about {\n      flex-grow: 1;\n      flex-shrink: 1;\n    } \n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0QkFBNEI7SUFDNUIsNERBQTREO0lBQzVELFlBQVk7SUFDWixxQ0FBcUM7SUFDckMsZ0JBQWdCO0lBQ2hCLG1DQUFtQztJQUNuQyxvQ0FBb0M7SUFDcEMsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLDBDQUEwQztDQUM3Qzs7QUFFRDtJQUNJLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLHNCQUFzQjtDQUMvRDs7QUFFRCxlQUFlOztBQUNmOzs7RUFHRSxlQUFlO0VBQ2YsYUFBYTtFQUNiLFFBQVE7RUFDUixPQUFPO0VBQ1AsWUFBWTtDQUNiOztBQUNEOztFQUVFLG1EQUFtRDtFQUNuRCxzQkFBc0I7RUFDdEIsMEJBQTBCO0NBQzNCOztBQUNEOztFQUVFLGlDQUF5QjtVQUF6Qix5QkFBeUI7RUFDekIsMkJBQTJCO0NBQzVCOztBQUVELGlCQUFpQjs7QUFDakI7SUFDSSwrQ0FBK0M7SUFDL0MsNEJBQTRCO0lBQzVCLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsZ0JBQWdCO0NBQ25COztBQUVEO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQixrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsaUJBQWlCO0NBQ3BCOztBQUVEO0lBQ0ksaUJBQWlCO0NBQ3BCOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxtQkFBbUI7SUFDbkIsaUVBQWlFO0lBQ2pFLG1CQUFtQjtJQUNuQixTQUFTO0NBQ1o7O0FBRUQ7SUFDSSxtQkFBbUI7SUFDbkIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixzQkFBc0I7Q0FDekI7O0FBRUQ7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixZQUFZO0lBQ1osWUFBWTtJQUNaLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsc0NBQThCO1lBQTlCLDhCQUE4QjtJQUM5QixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLHVCQUF1QjtJQUN2Qix3QkFBd0I7SUFDeEIsMkNBQTJDO0lBQzNDLFVBQVU7SUFDVixzQkFBc0I7SUFDdEIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsa0JBQWtCO0lBQ2xCLG1EQUEyQztJQUEzQywyQ0FBMkM7SUFBM0MsOERBQTJDO0lBQzNDLDBCQUFrQjtPQUFsQix1QkFBa0I7UUFBbEIsc0JBQWtCO1lBQWxCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLG1DQUFtQztDQUN0Qzs7QUFFRDtJQUNJLDZDQUE2QztJQUM3QyxnREFBd0M7WUFBeEMsd0NBQXdDO0NBQzNDOztBQUVEO0lBQ0ksNkNBQTZDO0lBQzdDLGdEQUF3QztZQUF4Qyx3Q0FBd0M7Q0FDM0M7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsWUFBWTtDQUNmOztBQUVEO0lBQ0ksb0JBQW9CO0lBQ3BCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLHdCQUF3QjtJQUN4QixhQUFhO0lBQ2IsZ0RBQXdDO0lBQXhDLHdDQUF3QztJQUF4QywyREFBd0M7SUFDeEMsWUFBWTtJQUNaLGdDQUFnQztDQUNuQzs7QUFFRDtJQUNJLFdBQVc7SUFDWCwrQkFBdUI7WUFBdkIsdUJBQXVCO0NBQzFCOztBQUVEO0lBQ0ksV0FBVztJQUNYLDhCQUFzQjtZQUF0QixzQkFBc0I7Q0FDekI7O0FBRUQsdUJBQXVCOztBQUN2QjtJQUNJO01BQ0UsdUJBQXVCO01BQ3ZCLHdCQUF3QjtNQUN4QixvQkFBb0I7TUFDcEIsbUJBQW1CO0tBQ3BCO0lBQ0Q7TUFDRSxvQkFBb0I7TUFDcEIsWUFBWTtLQUNiO0NBQ0o7O0FBRUQ7SUFDSTtNQUNFLG9CQUFvQjtNQUNwQiw4QkFBOEI7TUFDOUIsOEJBQThCO01BQzlCLGNBQWM7S0FDZjtJQUNEO01BQ0UsYUFBYTtNQUNiLGVBQWU7TUFDZixjQUFjO01BQ2Qsa0JBQWtCO01BQ2xCLFNBQVM7TUFDVCxhQUFhO0tBQ2Q7SUFDRDtNQUNFLGFBQWE7TUFDYixlQUFlO0tBQ2hCO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImh0bWwge1xuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JleTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tIHJpZ2h0LCAjMTgwY2FjKTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LWZhbWlseTogXCJRdWlja3NhbmRcIiwgc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgICAtd2Via2l0LWZvbnQtc21vb3RoaW5nOiBhbnRpYWxpYXNlZDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xuICAgIG1pbi13aWR0aDogMzAwcHg7XG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICAgIHRleHQtc2hhZG93OiAwIDNweCA1cHggcmdiYSgwLCAwLCAwLCAwLjEpO1xufVxuXG5hIHtcbiAgICBjb2xvcjogY3VycmVudENvbG9yOyBjdXJzb3I6IHBvaW50ZXI7IHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLyogMi4gU2hhcmVkICovXG4ud2FsbHBhcGVyLFxuLnBpY3R1cmUtc2hhZG93LFxuLnBpY3R1cmUtaW1hZ2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgaGVpZ2h0OiAxMDAlO1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmpvYixcbi5idXR0b24ge1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCIsIFwiUXVpY2tzYW5kXCIsIHNhbnMtc2VyaWY7XG4gIGxldHRlci1zcGFjaW5nOiAwLjNlbTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5idXR0b24sXG4uc29jaWFsIGEge1xuICB0cmFuc2Zvcm0tb3JpZ2luOiBjZW50ZXI7XG4gIHRyYW5zaXRpb24tZHVyYXRpb246IDEwMG1zO1xufVxuXG4vKiAzLiBTcGVjaWZpYyAqL1xuLndhbGxwYXBlciB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2ltYWdlcy9tb3VudGFpbnMuanBnXCIpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIG9wYWNpdHk6IDAuMjtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG59XG5cbi5jb250ZW50IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcbn1cblxuLnNpZGUge1xuICAgIG1heC1oZWlnaHQ6IDIwcmVtO1xuICAgIG1heC13aWR0aDogMjByZW07XG59XG5cbi5hYm91dCB7XG4gICAgbWF4LXdpZHRoOiAyNnJlbTtcbn1cblxuLnBpY3R1cmUge1xuICAgIHBhZGRpbmctdG9wOiAxMDAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnBpY3R1cmUtc2hhZG93IHtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogcmFkaWFsLWdyYWRpZW50KCMwMDAgMCUsIHJnYmEoMCwgMCwgMCwgMCkgNzAlKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxMCU7XG59XG5cbi5waWN0dXJlLWltYWdlIHtcbiAgICBib3JkZXItcmFkaXVzOiAxMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4ubmFtZSB7XG4gICAgZm9udC1zaXplOiAyLjI1cmVtO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjEyNTtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG59XG5cbi5qb2Ige1xuICAgIGNvbG9yOiAjZmZlNDc5O1xuICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcbn1cblxuLmhyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY0NzBmO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGhlaWdodDogMXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEuNXJlbTtcbiAgICBtYXJnaW4tdG9wOiAxLjVyZW07XG4gICAgdHJhbnNmb3JtLW9yaWdpbjogY2VudGVyIGxlZnQ7XG4gICAgd2lkdGg6IDRyZW07XG59XG5cbi5kZXNjcmlwdGlvbiB7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG59XG5cbi5jb250YWN0IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXRvcDogMS41cmVtO1xuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG5cbi5idXR0b24ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMjkwNDg2cHg7XG4gICAgYm94LXNoYWRvdzogMCAxcmVtIDJyZW0gcmdiYSgwLCAwLCAwLCAwLjIpO1xuICAgIGNvbG9yOnJlZDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBsaW5lLWhlaWdodDogMTtcbiAgICBwYWRkaW5nOiAxLjI1ZW0gMmVtO1xuICAgIHRleHQtc2hhZG93OiBub25lO1xuICAgIHRyYW5zaXRpb24tcHJvcGVydHk6IGJveC1zaGFkb3csIHRyYW5zZm9ybTtcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgd2lsbC1jaGFuZ2U6IGJveC1zaGFkb3csIHRyYW5zZm9ybTtcbn1cblxuLmJ1dHRvbjpob3ZlciB7XG4gICAgYm94LXNoYWRvdzogMCAxLjVyZW0gM3JlbSByZ2JhKDAsIDAsIDAsIDAuMik7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjAyKSB0cmFuc2xhdGVZKC00cHgpO1xufVxuXG4uYnV0dG9uOmFjdGl2ZSB7XG4gICAgYm94LXNoYWRvdzogMCAwLjVyZW0gMXJlbSByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjk4KSB0cmFuc2xhdGVZKC0ycHgpO1xufVxuXG4uc29jaWFsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbi10b3A6IDEuNXJlbTtcbn1cblxuLnNvY2lhbCBsaSB7XG4gICAgaGVpZ2h0OiAycmVtO1xuICAgIG1hcmdpbi1yaWdodDogMC41cmVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogMnJlbTtcbn1cblxuLnNvY2lhbCBhIHtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgaGVpZ2h0OiAycmVtO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIG9wYWNpdHk6IDAuNTtcbiAgICB0cmFuc2l0aW9uLXByb3BlcnR5OiBvcGFjaXR5LCB0cmFuc2Zvcm07XG4gICAgd2lkdGg6IDJyZW07XG4gICAgd2lsbC1jaGFuZ2U6IG9wYWNpdHksIHRyYW5zZm9ybTtcbn1cblxuLnNvY2lhbCBhOmhvdmVyIHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMS4yNSk7XG59XG5cbi5zb2NpYWwgYTphY3RpdmUge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpO1xufVxuXG4vKiA0LiBSZXNwb25zaXZlbmVzcyAqL1xuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzk5cHgpIHtcbiAgICAuY29udGVudCB7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgcGFkZGluZzogNXJlbSAzcmVtO1xuICAgIH1cbiAgICAuc2lkZSB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAzcmVtO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfSBcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogODAwcHgpIHtcbiAgICAuY29udGVudCB7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICAgIHBhZGRpbmc6IDRyZW07XG4gICAgfVxuICAgIC5zaWRlIHtcbiAgICAgIGZsZXgtZ3JvdzogMDtcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgICAgaGVpZ2h0OiAyMHJlbTtcbiAgICAgIG1hcmdpbi1sZWZ0OiA0cmVtO1xuICAgICAgb3JkZXI6IDI7XG4gICAgICB3aWR0aDogMjByZW07XG4gICAgfVxuICAgIC5hYm91dCB7XG4gICAgICBmbGV4LWdyb3c6IDE7XG4gICAgICBmbGV4LXNocmluazogMTtcbiAgICB9IFxufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"content\">\n  <aside class=\"side\">\n    <figure id=\"picture\" class=\"picture\">\n      <div class=\"picture-shadow\"></div>\n      <!-- Change the file names of your images -->\n      <img id=\"pictureImage\" class=\"picture-image\" src=\"../../assets/img/svg/037-barbershop.svg\" alt=\"Portrait of Akhilesh Sooji\"\n        width=\"80\" height=\"80\">\n    </figure>\n  </aside>\n  <main class=\"about\">\n    <h1 class=\"name\">\n      <!-- Change your name here -->\n      Welcome\n    </h1>\n    <p class=\"job\">\n      <!-- Change your title here -->\n\n    </p>\n    <hr class=\"hr\">\n    <div class=\"description\">\n      <p>\n        <!-- describe yourself here -->\n        I spend my time traveling the world,\n        helping startups and financial businesses\n        hire the best people.\n      </p>\n    </div>\n    <div class=\"contact\">\n      <a class=\"button\" [routerLink]=\"['/gender-selection']\">\n        Get in touch\n      </a>\n    </div>\n    <ul class=\"social\">\n      <!-- <li>\n                    <a href=\"https://twitter.com/@manoharsn\">\n                        <i class=\"fab fa-twitter\"></i>\n                    </a> \n                </li> -->\n      <li>\n        <!-- your facebook profile url -->\n        <a href=\"https://www.facebook.com/\">\n          <i class=\"fab fa-instagram\"></i>\n        </a>\n      </li>\n      <li>\n        <!-- your linkedin profile url -->\n        <a href=\"https://www.linkedin.com/in/\">\n          <i class=\"fab fa-linkedin\"></i>\n        </a>\n      </li>\n      <li>\n        <!-- your personal website url -->\n        <a href=\"#\">\n          <i class=\"fab fa-globe\"></i>\n        </a>\n      </li>\n    </ul>\n  </main>\n</div>\n<noscript>\n  <style type=\"text/css\">\n    .wallpaper {\n      animation-name: zoomOut;\n    }\n\n    .picture {\n      animation-name: dropIn;\n    }\n\n  </style>\n</noscript>\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/num-verify/num-verify.component.css":
/*!*****************************************************!*\
  !*** ./src/app/num-verify/num-verify.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL251bS12ZXJpZnkvbnVtLXZlcmlmeS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/num-verify/num-verify.component.html":
/*!******************************************************!*\
  !*** ./src/app/num-verify/num-verify.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div>\n    <form (ngSubmit)=\"userInfo(f)\" method=\"POST\" #f=\"ngForm\">\n      <div id=\"middle-wizard\">\n\n        <!-- First branch What Type of Project ============================== -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>Please enter your mobile number</h3>\n          </div>\n\n\n          <div class=\"col-lg-8\">\n            <div class=\"form-group\">\n              <label>Mobile number</label>\n              <input name=\"number\" mask='9999999999' maxlength=\"10\" ngModel #number=\"ngModel\" type=\"number\" class=\"form-control\" placeholder=\"Enter number\" value=\"{{dataObj?.phoneNumber}}\" (keyup)=\"onKey($event)\" required>\n              <small class=\"form-text text-muted\">We'll never share your number with anyone else.</small>\n            </div>\n\n\n            <div class=\"form-group\">\n              <label>Name</label>\n              <input name=\"name\" #name=\"ngModel\"  class=\"form-control\" type=\"text\" placeholder=\"Enter name\" ngModel value=\"{{dataObj?.name}}\">{{dataObj?.name}}\n            </div>  \n\n\n            <!-- <div style=\"text-align:center; padding-left: 400px\">\n              <select class=\"btn btn-info\" name=\"userList\" style=\"text-align:center\">\n                <option name=\"\" value=\"customer\">Customer</option>\n                <option name=\"\" value=\"employee\">Employee</option>\n              </select>\n            </div> -->\n\n\n            <!-- <div class=\"form-group\">\n              <label class=\"form-label\">Category Selection</label>\n              <select class=\"form-control\" [ngModel]=\"selectedRoles\" (ngModelChange)=\"onChange($event)\" name=\"sel2\">\n                  <option [ngValue]=\"i\" *ngFor=\"let i of selectRoles\">{{i}}</option>\n              \n             </select>\n            </div> -->\n            \n          </div>  \n\n\n          <div id=\"bottom-wizard\">\n            <button type=\"button\" name=\"backward\" routerLink=\"/\" class=\"backward\">Backward </button>\n\n            <button type=\"submit\" name=\"forward\" class=\"btn btn-primary\">Forward</button>\n          </div>\n\n\n        </div>\n      </div>\n    </form>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/num-verify/num-verify.component.ts":
/*!****************************************************!*\
  !*** ./src/app/num-verify/num-verify.component.ts ***!
  \****************************************************/
/*! exports provided: NumVerifyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumVerifyComponent", function() { return NumVerifyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _num_verify_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./num-verify.service */ "./src/app/num-verify/num-verify.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { empty } from 'rxjs';
var NumVerifyComponent = /** @class */ (function () {
    function NumVerifyComponent(numService, router) {
        this.numService = numService;
        this.router = router;
        this.selectRoles = ["customer", "employer"];
        this.selectedRoles = this.selectRoles[0];
    }
    NumVerifyComponent.prototype.ngOnInit = function () { };
    NumVerifyComponent.prototype.userInfo = function (form) {
        var obj = {
            phoneNumber: form.value.number,
            name: form.value.name,
        };
        this.info = obj;
        this.getIdDb = this.dataObj;
        this.router.navigateByUrl('/confirmation');
        if (this.info.name && this.info.phoneNumber) {
            localStorage.setItem(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].userInfo, JSON.stringify(obj));
            console.log("localStorage", localStorage.getItem(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].userInfo));
        }
        else {
            if (this.getIdDb.name && this.getIdDb.phoneNumber) {
                localStorage.setItem(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].getIdDb, this.getIdDb._id);
                console.log("localStorage", localStorage.getItem(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].getIdDb));
            }
        }
    };
    // userDetail(form:NgForm) {
    //   const object = {
    //     phoneNumber: form.value.number,
    //     name: form.value.name
    //   }
    //   //console.log("object", object)
    //   //console.log(this.dataObj)
    //   this.router.navigateByUrl('/confirmation')
    //   if(object.name.length <= 1 && this.dataObj._id.length > 1) {
    //     //console.log("from if condition", this.dataObj)
    //     // let objectData = {phoneNumber: this.dataObj.phoneNumber, name: this.dataObj.name}
    //     let objectId = this.dataObj._id
    //     //console.log("objectData", objectData)
    //     localStorage.setItem(environment.userInfo,  objectId)
    //     console.log("localStorage", localStorage.getItem(environment.userInfo))
    //   } else {
    //     //console.log(object);
    //     localStorage.setItem(environment.userInfo, JSON.stringify(object))
    //     console.log("localStorage", localStorage.getItem(environment.userInfo))
    //   }
    // }
    // 
    NumVerifyComponent.prototype.onChange = function (newValue) {
        //console.log(newValue);
        this.selectedRoles = newValue;
    };
    //OnKey function is used to hit database and get saved data.
    NumVerifyComponent.prototype.onKey = function (event) {
        this.inputValue = event.target.value;
        this.getUsers();
        console.log('inputValue', this.inputValue);
    };
    //OnSignIn function is used to register the customer information.
    // onSignIn(form: NgForm) {
    //   const obj = {
    //     phoneNumber: form.value.number,
    //     name: form.value.name,
    //     type: this.selectedRoles
    //   }
    //   this.token = this.numService.numVerifyUser(obj).subscribe(resp => {
    //     console.log(resp);
    //     this.userId = resp;
    //     //console.log(this.userId)
    //     localStorage.setItem(environment.user, JSON.stringify(resp));
    //     this.router.navigate(['confirmation']);
    //   });
    // }
    //GetUsers function takes the value from onKey & search data in db.
    NumVerifyComponent.prototype.getUsers = function () {
        var _this = this;
        this.inputValue;
        console.log("this.inputValue", this.inputValue);
        this.numService.verifyNum(this.inputValue).subscribe(function (response) {
            console.log("response", response);
            _this.dataObj = response.customers[0];
            console.log('this dATA', _this.dataObj);
        }, function (error) { return console.log(error); });
    };
    NumVerifyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-num-verify',
            template: __webpack_require__(/*! ./num-verify.component.html */ "./src/app/num-verify/num-verify.component.html"),
            styles: [__webpack_require__(/*! ./num-verify.component.css */ "./src/app/num-verify/num-verify.component.css")],
            providers: [_num_verify_service__WEBPACK_IMPORTED_MODULE_1__["NumVerifyService"]]
        }),
        __metadata("design:paramtypes", [_num_verify_service__WEBPACK_IMPORTED_MODULE_1__["NumVerifyService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NumVerifyComponent);
    return NumVerifyComponent;
}());



/***/ }),

/***/ "./src/app/num-verify/num-verify.service.ts":
/*!**************************************************!*\
  !*** ./src/app/num-verify/num-verify.service.ts ***!
  \**************************************************/
/*! exports provided: NumVerifyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumVerifyService", function() { return NumVerifyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var NumVerifyService = /** @class */ (function () {
    //private signup = 'http://52.66.249.189:3011/userRegister';
    function NumVerifyService(httpClient) {
        this.httpClient = httpClient;
    }
    // numVerifyUser(obj): Observable<any> {
    //     let object = obj;
    //     return this.httpClient.post<any>(this.signup, object, httpOptions)
    // }
    NumVerifyService.prototype.verifyNum = function (num) {
        return this.httpClient.get("http://52.66.249.189:3011/customers?phoneNumber=" + num, httpOptions);
    };
    NumVerifyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], NumVerifyService);
    return NumVerifyService;
}());



/***/ }),

/***/ "./src/app/service-selection/service-selection.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/service-selection/service-selection.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZpY2Utc2VsZWN0aW9uL3NlcnZpY2Utc2VsZWN0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/service-selection/service-selection.component.html":
/*!********************************************************************!*\
  !*** ./src/app/service-selection/service-selection.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div id=\"wizard_container\">\n    <form name=\"example-1\" id=\"wrapped\" method=\"POST\" ngform>\n      <input id=\"website\" name=\"website\" type=\"text\" value=\"\">\n      <div id=\"middle-wizard\">\n\n        <!-- This is the hearder division where title is present -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>Please select one of the following</h3>\n          </div>\n\n          <!-- This is the option selection for the gender -->\n          <div class=\"row\">\n            <div class=\"col-sm-2\" *ngFor=\"let cat of categories\">\n              <div class=\"item\">\n                  \n                      \n                      <label for=\"category\" (click)=\"setService(cat.name)\">\n\n                        <img src=\"./assets/img/svg/014-hair-1.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>{{cat.name}}</strong>\n                      \n                      </label>\n                    \n              </div>\n\n            </div>\n\n\n\n            <!-- <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"beared\" name=\"beared\" type=\"radio\" value=\"beared\" (click)=\"getBeared()\" class=\"required\">\n                <label for=\"beared\"><img src=\"./assets/img/svg/013-beard.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Beared</strong></label>\n              </div>\n            </div>\n\n\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"facial\" name=\"facial\" type=\"radio\" value=\"facial\" (click)=\"getFacial()\" class=\"required\">\n                <label for=\"facial\"><img src=\"./assets/img/svg/030-lotion.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Facial</strong></label>\n              </div>\n            </div>\n\n\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"bleach\" name=\"bleach\" type=\"radio\" value=\"bleach\" (click)=\"getBleach()\" class=\"required\">\n                <label for=\"bleach\"><img src=\"./assets/img/svg/032-cream.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Bleach</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"dtan\" name=\"dtan\" type=\"radio\" value=\"dtan\" (click)=\"getDtan()\" class=\"required\">\n                <label for=\"dtan\"><img src=\"./assets/img/svg/man-3.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>D-Tan</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"massage\" name=\"massage\" type=\"radio\" value=\"massage\" (click)=\"getMassage()\" class=\"required\">\n                <label for=\"massage\"><img src=\"./assets/img/svg/004-spray-1.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Massage</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"waxing\" name=\"waxing\" type=\"radio\" value=\"waxing\" (click)=\"getWaxing()\" class=\"required\">\n                <label for=\"waxing\"><img src=\"./assets/img/svg/005-blade.svg\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Waxing</strong></label>\n              </div>\n            </div>\n\n            <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"pedicure\" name=\"pedicure\" type=\"radio\" value=\"pedicure\" (click)=\"getPedicure()\" class=\"required\">\n                <label for=\"predicure\"><img src=\"./assets/img/svg/download (1).png\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Pedicure</strong></label>\n              </div>\n            </div> -->\n\n\n            <!-- <div class=\"col-sm-2\">\n              <div class=\"item\">\n                <input id=\"manicure\" name=\"manicure\" type=\"radio\" value=\"manicure\" (click)=\"getManicure()\" class=\"required\">\n                <label for=\"manicure\"><img src=\"./assets/img/svg/apple-touch-icon-57x57-precomposed.png\" alt=\"\" style=\"width:80px; height:80px;\"><strong>Manicure</strong></label>\n              </div>\n            </div> -->\n\n            <div class=\"col-lg-12\">\n              <div id=\"bottom-wizard\">\n\n                <button type=\"button\" name=\"backward\" routerLink=\"/stage-selection\" class=\"backward\">Backward </button>\n                <!-- <button type=\"button\" name=\"forward\" routerLink=\"/num-verify\" class=\"forward\">Forward</button> -->\n\n              </div>\n            </div>\n\n\n\n\n\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/service-selection/service-selection.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/service-selection/service-selection.component.ts ***!
  \******************************************************************/
/*! exports provided: ServiceSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceSelectionComponent", function() { return ServiceSelectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _service_selection_service_selection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service-selection/service-selection.service */ "./src/app/service-selection/service-selection.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceSelectionComponent = /** @class */ (function () {
    function ServiceSelectionComponent(router, categorylist) {
        this.router = router;
        this.categorylist = categorylist;
    }
    ServiceSelectionComponent.prototype.setService = function (name) {
        //console.log('name', name);
        this.selectedService = name;
        //set the service in local storage
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Service, name);
        this.router.navigate(["/num-verify"]);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Service));
    };
    ServiceSelectionComponent.prototype.ngOnInit = function () {
        this.getCategoryList();
    };
    ServiceSelectionComponent.prototype.getCategoryList = function () {
        var _this = this;
        this.categorylist.getCategoryList().subscribe(function (response) {
            console.log(response.data);
            _this.categories = response.data;
        }, function (error) { return console.log(error); });
    };
    ServiceSelectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-service-selection',
            template: __webpack_require__(/*! ./service-selection.component.html */ "./src/app/service-selection/service-selection.component.html"),
            styles: [__webpack_require__(/*! ./service-selection.component.css */ "./src/app/service-selection/service-selection.component.css")],
            providers: [_service_selection_service_selection_service__WEBPACK_IMPORTED_MODULE_3__["ServiceSelectionService"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _service_selection_service_selection_service__WEBPACK_IMPORTED_MODULE_3__["ServiceSelectionService"]])
    ], ServiceSelectionComponent);
    return ServiceSelectionComponent;
}());



/***/ }),

/***/ "./src/app/service-selection/service-selection.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/service-selection/service-selection.service.ts ***!
  \****************************************************************/
/*! exports provided: ServiceSelectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceSelectionService", function() { return ServiceSelectionService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var ServiceSelectionService = /** @class */ (function () {
    function ServiceSelectionService(httpClient) {
        this.httpClient = httpClient;
        this.category = 'http://52.66.249.189:3011/categories';
    }
    ServiceSelectionService.prototype.serviceSelection = function (obj) {
        var object = obj;
        return this.httpClient.post("http://52.66.249.189:3011/categories", object, httpOptions);
    };
    ServiceSelectionService.prototype.getCategoryList = function () {
        return this.httpClient.get("http://52.66.249.189:3011/categories", httpOptions);
    };
    ServiceSelectionService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ServiceSelectionService);
    return ServiceSelectionService;
}());



/***/ }),

/***/ "./src/app/stage-selection/stage-selection.component.css":
/*!***************************************************************!*\
  !*** ./src/app/stage-selection/stage-selection.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWdlLXNlbGVjdGlvbi9zdGFnZS1zZWxlY3Rpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/stage-selection/stage-selection.component.html":
/*!****************************************************************!*\
  !*** ./src/app/stage-selection/stage-selection.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div id=\"wizard_container\">\n    <form name=\"example-1\" id=\"wrapped\" method=\"POST\" ngform>\n      <input id=\"website\" name=\"website\" type=\"text\" value=\"\">\n      <!-- Leave input above for security protection, read docs for details -->\n      <div id=\"middle-wizard\">\n\n        <!-- First branch What Type of Project ============================== -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>What Type of Project do you need?</h3>\n            <p>Selection with Branch (First Branch). Web Development have a <strong>Second Branch</strong>.</p>\n          </div>\n\n\n          <div class=\"row\">\n            <div class=\"col-lg-6\">\n              <div class=\"item\">\n                <input id=\"adult\" type=\"radio\" name=\"adult\" (click)=\"getAdult()\" value=\"adult\" class=\"required\">\n                <label for=\"adult\"><img src=\"/assets/img/svg/man-1.svg\" style=\"width:120px; height:120px;\" alt=\"\"><strong>Adult</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-lg-6\">\n              <div class=\"item\">\n                <input id=\"kid\" name=\"kid\" type=\"radio\" value=\"kid\" (click)=\"getKid()\" class=\"required\">\n                <label for=\"kid\"><img src=\"/assets/img/svg/boy.svg\" style=\"width:120px; height:120px;\" alt=\"\"><strong>Kids</strong></label>\n              </div>\n            </div>\n            <div class=\"col-lg-12\">\n            <div id=\"bottom-wizard\">\n              <button type=\"button\" name=\"backward\" routerLink=\"/gender-selection\" class=\"backward\">Backward </button>\n\n\n              <!-- <button type=\"button\" name=\"forward\" routerLink= \"/container3\" class=\"forward\">Forward</button> -->\n            </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/stage-selection/stage-selection.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/stage-selection/stage-selection.component.ts ***!
  \**************************************************************/
/*! exports provided: StageSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StageSelectionComponent", function() { return StageSelectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _stage_selection_stage_selection_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../stage-selection/stage-selection.service */ "./src/app/stage-selection/stage-selection.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StageSelectionComponent = /** @class */ (function () {
    function StageSelectionComponent(router, stageSel) {
        this.router = router;
        this.stageSel = stageSel;
    }
    StageSelectionComponent.prototype.getAdult = function () {
        this.b = "Adult";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage, this.b);
        this.router.navigate(["/service-selection"]);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage));
    };
    StageSelectionComponent.prototype.getKid = function () {
        this.b = "Kid";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage, this.b);
        this.router.navigate(["/service-selection"]);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage));
    };
    StageSelectionComponent.prototype.ngOnInit = function () {
    };
    StageSelectionComponent.prototype.getStageSel = function () {
        var _this = this;
        this.stageSel.stageSel().subscribe(function (response) {
            console.log(response.data);
            _this.stageList = response.data;
        }, function (error) { return console.log(error); });
    };
    StageSelectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stage-selection',
            template: __webpack_require__(/*! ./stage-selection.component.html */ "./src/app/stage-selection/stage-selection.component.html"),
            styles: [__webpack_require__(/*! ./stage-selection.component.css */ "./src/app/stage-selection/stage-selection.component.css")],
            providers: [_stage_selection_stage_selection_service__WEBPACK_IMPORTED_MODULE_3__["StageSelection"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _stage_selection_stage_selection_service__WEBPACK_IMPORTED_MODULE_3__["StageSelection"]])
    ], StageSelectionComponent);
    return StageSelectionComponent;
}());



/***/ }),

/***/ "./src/app/stage-selection/stage-selection.service.ts":
/*!************************************************************!*\
  !*** ./src/app/stage-selection/stage-selection.service.ts ***!
  \************************************************************/
/*! exports provided: StageSelection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StageSelection", function() { return StageSelection; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-type': 'application/json'
    })
};
var StageSelection = /** @class */ (function () {
    function StageSelection(httpClient) {
        this.httpClient = httpClient;
        this.stageSelection = 'http://52.66.249.189:3011/leads';
    }
    StageSelection.prototype.stageSelction = function (obj) {
        var object = obj;
        return this.httpClient.post(this.stageSelection, object, httpOptions);
    };
    StageSelection.prototype.stageSel = function () {
        return this.httpClient.get(this.stageSelection, httpOptions);
    };
    StageSelection = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], StageSelection);
    return StageSelection;
}());



/***/ }),

/***/ "./src/app/stages-selection/stages-selection.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/stages-selection/stages-selection.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N0YWdlcy1zZWxlY3Rpb24vc3RhZ2VzLXNlbGVjdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/stages-selection/stages-selection.component.html":
/*!******************************************************************!*\
  !*** ./src/app/stages-selection/stages-selection.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div id=\"wizard_container\">\n    <form name=\"example-1\" id=\"wrapped\" method=\"POST\">\n      <input id=\"website\" name=\"website\" type=\"text\">\n      <div id=\"middle-wizard\">\n\n        <!-- This is the hearder division where title is present -->\n        <div class=\"step\" data-state=\"branchtype\">\n          <div class=\"question_title\">\n            <h3>Please select one of the following</h3>\n          </div>\n\n          <!-- This is the option selection for the gender -->\n          <div class=\"row\">\n            <div class=\"col-lg-6\">\n              <div class=\"item\">\n                <input id=\"Adults\" name=\"Adults\" type=\"radio\" value=\"Adults\" (click)=\"getAdults()\" class=\"required\">\n                <label for=\"Adults\"><img src=\"./assets/img/svg/girl-1.svg\" alt=\"\" style=\"width:120px; height:120px;\"><strong>Adults</strong></label>\n              </div>\n            </div>\n\n\n            <div class=\"col-lg-6\">\n                <div class=\"item\">\n                  <input id=\"kids\" name=\"kids\" type=\"radio\" value=\"kids\" (click)=\"getKids()\" class=\"required\"> \n                  <label for=\"kids\"><img src=\"./assets/img/svg/girl.svg\" alt=\"\" style=\"width:120px; height:120px;\"><strong>Kids</strong></label>\n                </div>\n              </div>\n\n\n\n            <div class=\"col-lg-12\">\n              <div id=\"bottom-wizard\">\n\n\n                <!-- <button type=\"button\" name=\"forward\" routerLink=\"/container2\" class=\"forward\">Forward</button> -->\n\n                <button type=\"button\" name=\"backward\" routerLink=\"/gender-selection\" class=\"backward\">Backward </button>\n\n              </div>\n            </div>\n\n\n\n\n\n          </div>\n        </div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/stages-selection/stages-selection.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/stages-selection/stages-selection.component.ts ***!
  \****************************************************************/
/*! exports provided: StagesSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StagesSelectionComponent", function() { return StagesSelectionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _src_environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StagesSelectionComponent = /** @class */ (function () {
    function StagesSelectionComponent(router) {
        this.router = router;
    }
    StagesSelectionComponent.prototype.getAdults = function () {
        this.b = "Adult";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage, this.b);
        this.router.navigate(["/service-selection"]);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stages));
    };
    StagesSelectionComponent.prototype.getKids = function () {
        this.b = "Kid";
        localStorage.setItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stage, this.b);
        this.router.navigate(["/service-selection"]);
        console.log(localStorage.getItem(_src_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].Stages));
    };
    StagesSelectionComponent.prototype.ngOnInit = function () {
    };
    StagesSelectionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stages-selection',
            template: __webpack_require__(/*! ./stages-selection.component.html */ "./src/app/stages-selection/stages-selection.component.html"),
            styles: [__webpack_require__(/*! ./stages-selection.component.css */ "./src/app/stages-selection/stages-selection.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], StagesSelectionComponent);
    return StagesSelectionComponent;
}());



/***/ }),

/***/ "./src/app/thank-you/thank-you.component.css":
/*!***************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RoYW5rLXlvdS90aGFuay15b3UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/thank-you/thank-you.component.html":
/*!****************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-xs-center\">\n    <h1 class=\"display-3\">Thank You!</h1>\n    <p class=\"lead\"><strong>Your request is been queued</strong> for further details, go through the message that you will recieve in a momonet.</p>\n    <hr>\n    <p>\n      Having trouble? <a href=\"\">Contact us</a>\n    </p>\n    <div>\n      <h2>Hello {{latestLead?.user.name}}, thank you for your patience.</h2>\n      <h2>Your token number is {{latestLead?.token}}. Please wait for sometime as our executive will reply you.</h2>\n      <!-- <h2>{{latestLead?.phoneNumber}}</h2> -->\n    </div>\n    <p class=\"lead\">\n      <button type=\"button\" name=\"forward\" routerLink=\"/\" class=\"forward\">Continue to homepage</button>\n    </p>\n  </div>\n"

/***/ }),

/***/ "./src/app/thank-you/thank-you.component.ts":
/*!**************************************************!*\
  !*** ./src/app/thank-you/thank-you.component.ts ***!
  \**************************************************/
/*! exports provided: ThankYouComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThankYouComponent", function() { return ThankYouComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _thank_you_thank_you_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../thank-you/thank-you.service */ "./src/app/thank-you/thank-you.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ThankYouComponent = /** @class */ (function () {
    function ThankYouComponent(router, genLead) {
        this.router = router;
        this.genLead = genLead;
    }
    ThankYouComponent.prototype.ngOnInit = function () {
        this.getLead();
    };
    ThankYouComponent.prototype.getLead = function () {
        var _this = this;
        this.genLead.getLeads().subscribe(function (response) {
            console.log("this.latestLead", response.latest[0]);
            _this.latestLead = response.latest[0];
            //console.log(response.data[0]) 
        }, function (error) { return console.log(error); });
    };
    ThankYouComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-thank-you',
            template: __webpack_require__(/*! ./thank-you.component.html */ "./src/app/thank-you/thank-you.component.html"),
            styles: [__webpack_require__(/*! ./thank-you.component.css */ "./src/app/thank-you/thank-you.component.css")],
            providers: [_thank_you_thank_you_service__WEBPACK_IMPORTED_MODULE_2__["LeadService"]]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _thank_you_thank_you_service__WEBPACK_IMPORTED_MODULE_2__["LeadService"]])
    ], ThankYouComponent);
    return ThankYouComponent;
}());



/***/ }),

/***/ "./src/app/thank-you/thank-you.service.ts":
/*!************************************************!*\
  !*** ./src/app/thank-you/thank-you.service.ts ***!
  \************************************************/
/*! exports provided: LeadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeadService", function() { return LeadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var LeadService = /** @class */ (function () {
    function LeadService(httpClient) {
        this.httpClient = httpClient;
        this.latestLead = 'http://52.66.249.189:3011/leads';
    }
    LeadService.prototype.getLeads = function () {
        return this.httpClient.get('http://52.66.249.189:3011/leads', httpOptions);
    };
    LeadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LeadService);
    return LeadService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false,
    apiUrl: 'http://52.66.249.189:3011',
    Gender: 'gender',
    Stage: 'stage',
    Stages: 'stages',
    Service: 'service',
    user: 'user',
    Num: 'number',
    userInfo: 'info',
    getIdDb: 'getid',
    counter: 'counts',
    Leads: 'leads'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/akhilesh/angular-project/angular-project/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map