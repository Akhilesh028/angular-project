import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()

export class NumVerifyService {
    //private signup = 'http://52.66.249.189:3011/userRegister';
    constructor(private httpClient: HttpClient) { }
    // numVerifyUser(obj): Observable<any> {
    //     let object = obj;
    //     return this.httpClient.post<any>(this.signup, object, httpOptions)
    // }

    verifyNum(num) {
        return this.httpClient.get<any>(`http://52.66.249.189:3011/customers?phoneNumber=${num}`, httpOptions);
    }
}


