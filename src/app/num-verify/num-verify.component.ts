import { Component, OnInit } from '@angular/core';
import { NumVerifyService } from './num-verify.service';
import { NgForm } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { environment } from 'src/environments/environment';
//import { empty } from 'rxjs';


@Component({
  selector: 'app-num-verify',
  templateUrl: './num-verify.component.html',
  styleUrls: ['./num-verify.component.css'],
  providers: [NumVerifyService]
})

export class NumVerifyComponent implements OnInit {
  public token;
  public user;
  public dataObj;
  public inputValue;

  list: any;
  userId: any;
  info: any;
  getIdDb: any;

  selectRoles = ["customer", "employer"];
  selectedRoles = this.selectRoles[0];

  constructor(
    private numService: NumVerifyService,
    private router: Router) { }


  ngOnInit() { }


  userInfo(form: NgForm) {
    const obj = {
      phoneNumber: form.value.number,
      name: form.value.name,
      //customerId: form.value.customerId
    }
    this.info = obj
    this.getIdDb = this.dataObj
    this.router.navigateByUrl('/confirmation')
    if (this.info.name && this.info.phoneNumber) {
      localStorage.setItem(environment.userInfo, JSON.stringify(obj));
      console.log("localStorage", localStorage.getItem(environment.userInfo));
    } else {
      if (this.getIdDb.name && this.getIdDb.phoneNumber) {
        localStorage.setItem(environment.getIdDb, this.getIdDb._id)
        console.log("localStorage", localStorage.getItem(environment.getIdDb));
      }
    }
  }

  // userDetail(form:NgForm) {
  //   const object = {
  //     phoneNumber: form.value.number,
  //     name: form.value.name
  //   }

  //   //console.log("object", object)
  //   //console.log(this.dataObj)
  //   this.router.navigateByUrl('/confirmation')

  //   if(object.name.length <= 1 && this.dataObj._id.length > 1) {
  //     //console.log("from if condition", this.dataObj)
  //     // let objectData = {phoneNumber: this.dataObj.phoneNumber, name: this.dataObj.name}
  //     let objectId = this.dataObj._id
  //     //console.log("objectData", objectData)
  //     localStorage.setItem(environment.userInfo,  objectId)
  //     console.log("localStorage", localStorage.getItem(environment.userInfo))
  //   } else {
  //     //console.log(object);
  //     localStorage.setItem(environment.userInfo, JSON.stringify(object))
  //     console.log("localStorage", localStorage.getItem(environment.userInfo))
  //   }
  // }



  // 
  onChange(newValue) {
    //console.log(newValue);
    this.selectedRoles = newValue;
  }

  //OnKey function is used to hit database and get saved data.
  onKey(event) {
    this.inputValue = event.target.value;
    this.getUsers();
    console.log('inputValue', this.inputValue);
  }

  //OnSignIn function is used to register the customer information.
  // onSignIn(form: NgForm) {
  //   const obj = {
  //     phoneNumber: form.value.number,
  //     name: form.value.name,
  //     type: this.selectedRoles
  //   }
  //   this.token = this.numService.numVerifyUser(obj).subscribe(resp => {
  //     console.log(resp);
  //     this.userId = resp;
  //     //console.log(this.userId)
  //     localStorage.setItem(environment.user, JSON.stringify(resp));

  //     this.router.navigate(['confirmation']);
  //   });
  // }

  //GetUsers function takes the value from onKey & search data in db.
  getUsers() {
    this.inputValue;
    console.log("this.inputValue", this.inputValue);
    this.numService.verifyNum(this.inputValue).subscribe(
      (response) => {
        console.log("response", response)
        this.dataObj = response.customers[0];
        console.log('this dATA', this.dataObj)
      },
      error => console.log(error))

      
  }
}

