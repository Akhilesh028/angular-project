import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
 

@Injectable()

export class LeadService {

    latestLead = 'http://52.66.249.189:3011/leads';

    constructor(private httpClient: HttpClient) { }

    getLeads(){
        return this.httpClient.get<any>('http://52.66.249.189:3011/leads', httpOptions);
    }


}