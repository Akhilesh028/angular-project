import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LeadService } from "../thank-you/thank-you.service";
import { environment } from "../../../src/environments/environment"


@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.css'],
  providers: [LeadService]
})
export class ThankYouComponent implements OnInit {

  public latestLead;

  constructor(
    private router: Router,
    private genLead:LeadService) {}

  ngOnInit() {
   this.getLead()
    
  }

  getLead() {
    this.genLead.getLeads().subscribe((response) => {
      console.log("this.latestLead", response.latest[0])
    this.latestLead = response.latest[0];
      //console.log(response.data[0]) 
    },
    error => console.log(error));
  }
}
