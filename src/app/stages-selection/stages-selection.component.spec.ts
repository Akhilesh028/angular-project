import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StagesSelectionComponent } from './stages-selection.component';

describe('StagesSelectionComponent', () => {
  let component: StagesSelectionComponent;
  let fixture: ComponentFixture<StagesSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StagesSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StagesSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
