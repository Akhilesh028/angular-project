import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {environment} from "../../../src/environments/environment"

@Component({
  selector: 'app-stages-selection',
  templateUrl: './stages-selection.component.html',
  styleUrls: ['./stages-selection.component.css']
})
export class StagesSelectionComponent implements OnInit {


  b: any;
  

  getAdults() {
    this.b = "Adult";
    localStorage.setItem(environment.Stage, this.b);
    this.router.navigate(["/service-selection"])
    console.log(localStorage.getItem(environment.Stages));

  }

  getKids() {
    this.b = "Kid";
    localStorage.setItem(environment.Stage, this.b);
    this.router.navigate(["/service-selection"])
    console.log(localStorage.getItem(environment.Stages));
  }


  constructor(private router: Router) { }

  ngOnInit() {
  }

}
