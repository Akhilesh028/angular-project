import { Component, OnInit } from '@angular/core';
import { environment } from "../../../src/environments/environment"
import { ConfirmationService } from './confirmation.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css'],
  providers: [ConfirmationService]
})
export class ConfirmationComponent implements OnInit {

  dataMale: any;
  dataStage: any;
  dataStages: any;
  dataService: any;
  userInfo: any;

  public userobj;
  public tokenService;

  constructor(private catService: ConfirmationService,
    private router: Router) { }
  

  ngOnInit() {
    this.getMaleData()
    this.getStageData()
    this.getService()
    }


  onSubmit() {
    const leadObj = {
      gender: this.dataMale,
      stage: this.dataStage,
      service: this.dataService,
      userinformat : localStorage.getItem(environment.userInfo),
      userId : localStorage.getItem(environment.getIdDb)
    }
    return this.catService.categorys(leadObj).subscribe(resp => {
      console.log(resp);
      this.router.navigate(['/thankyou'])
      localStorage.removeItem(environment.Gender)
      localStorage.removeItem(environment.Stage)  
      localStorage.removeItem(environment.Service)
      localStorage.removeItem(environment.user)
      localStorage.removeItem(environment.getIdDb)
      localStorage.removeItem(environment.userInfo)
      localStorage.removeItem(environment.Stages)
    })
  }

  getMaleData() {
    this.dataMale = localStorage.getItem(environment.Gender);
    console.log(this.dataMale);
  }

  getStageData() {
    this.dataStage = localStorage.getItem(environment.Stage);
    console.log(this.dataStage);
  
    }

  getService() {
    this.dataService = localStorage.getItem(environment.Service);
    console.log(this.dataService);
  }
}


