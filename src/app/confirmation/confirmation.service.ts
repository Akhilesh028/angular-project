import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':'application/json'
    })
};

@Injectable()

export class ConfirmationService {
    private lead = 'http://52.66.249.189:3011/leads';  

    constructor(private httpClient: HttpClient) {}

    categorys(leadObj): Observable<any> {
        let objects = leadObj;
        return this.httpClient.post<any>(this.lead, objects, httpOptions)
    }
}