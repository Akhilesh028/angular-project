import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from "../../../src/environments/environment";


@Component({
  selector: 'app-gender-selection',
  templateUrl: './gender-selection.component.html',
  styleUrls: ['./gender-selection.component.css']
})

export class GenderSelectionComponent implements OnInit {
  public customersList;
  a: any;



  getMale() {
    this.a = "Male";
    localStorage.setItem(environment.Gender, this.a);
    console.log(localStorage.getItem(environment.Gender));

    this.router.navigate(["/stage-selection"])
  }


  getFemale() {
    this.a = "Female";
    localStorage.setItem(environment.Gender, this.a)
    console.log(localStorage.getItem(environment.Gender));

    this.router.navigate(["/stages-selection"])
  }


  constructor(private router: Router) { }

  ngOnInit() {}

}


