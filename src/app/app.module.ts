import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { StageSelectionComponent } from './stage-selection/stage-selection.component';
import { ServiceSelectionComponent } from './service-selection/service-selection.component';
import { NumVerifyComponent } from './num-verify/num-verify.component';
import { GenderSelectionComponent } from './gender-selection/gender-selection.component';
import { HomeComponent } from './home/home.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { StagesSelectionComponent } from './stages-selection/stages-selection.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    StageSelectionComponent,
    ServiceSelectionComponent,
    NumVerifyComponent,
    GenderSelectionComponent,
    HomeComponent,
    ConfirmationComponent,
    ThankYouComponent,
    StagesSelectionComponent,
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
