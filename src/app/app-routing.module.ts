import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StageSelectionComponent } from './stage-selection/stage-selection.component';
import { ServiceSelectionComponent } from './service-selection/service-selection.component';

import { NumVerifyComponent } from './num-verify/num-verify.component';
import { GenderSelectionComponent } from './gender-selection/gender-selection.component';
import { HomeComponent } from './home/home.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { StagesSelectionComponent } from './stages-selection/stages-selection.component';


const routes: Routes = [
  // { path: '', redirectTo: '/gender-selection', pathMatch: 'full' },
  { path: 'stage-selection', component: StageSelectionComponent },
  { path: 'service-selection', component: ServiceSelectionComponent },
  { path: 'stages-selection', component: StagesSelectionComponent },
  { path: 'num-verify', component: NumVerifyComponent },
  { path: 'gender-selection', component: GenderSelectionComponent },
  { path: 'confirmation', component: ConfirmationComponent },
  { path: 'thankyou', component: ThankYouComponent },
  

  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
