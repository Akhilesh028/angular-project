import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from "../../../src/environments/environment";
import { StageSelection } from '../stage-selection/stage-selection.service';


@Component({
  selector: 'app-stage-selection',
  templateUrl: './stage-selection.component.html',
  styleUrls: ['./stage-selection.component.css'],
  providers: [StageSelection]
})

export class StageSelectionComponent implements OnInit {
  public stageList;
  b: any;


  getAdult() {
    this.b = "Adult";
    localStorage.setItem(environment.Stage, this.b);
    this.router.navigate(["/service-selection"])
    console.log(localStorage.getItem(environment.Stage));

  }

  getKid() {
    this.b = "Kid";
    localStorage.setItem(environment.Stage, this.b)
    this.router.navigate(["/service-selection"])
    console.log(localStorage.getItem(environment.Stage));


  }

  constructor(private router: Router,
    private stageSel: StageSelection) { }

  ngOnInit() {

  }

  getStageSel() {
    this.stageSel.stageSel().subscribe((response) => {
      console.log(response.data);
      this.stageList = response.data;
    },
      error => console.log(error));
  }

}