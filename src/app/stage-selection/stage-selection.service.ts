import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-type': 'application/json'
    })
};

@Injectable()

export class StageSelection {

    private stageSelection = 'http://52.66.249.189:3011/leads';

    constructor(private httpClient:HttpClient) {}
    

    stageSelction(obj): Observable<any> {
        let object = obj;
        return this.httpClient.post<any>(this.stageSelection, object, httpOptions)
    }
    
    stageSel(){
        return this.httpClient.get<any>(this.stageSelection, httpOptions);
    }
} 