import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()

export class ServiceSelectionService {

    private category = 'http://52.66.249.189:3011/categories';

    constructor(private httpClient: HttpClient) { }


    serviceSelection(obj): Observable<any> {
        let object = obj;
        return this.httpClient.post<any>("http://52.66.249.189:3011/categories", object, httpOptions)
    }



    getCategoryList() {
        return this.httpClient.get<any>("http://52.66.249.189:3011/categories", httpOptions);
    }

}
