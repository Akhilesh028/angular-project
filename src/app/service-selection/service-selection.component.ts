import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from "../../../src/environments/environment"
import { ServiceSelectionService } from "../service-selection/service-selection.service";


@Component({
  selector: 'app-service-selection',
  templateUrl: './service-selection.component.html',
  styleUrls: ['./service-selection.component.css'],
  providers: [ServiceSelectionService]
})

export class ServiceSelectionComponent implements OnInit {


  c: any;
  public categories;
  selectedService: any;


  setService(name){
    //console.log('name', name);
    this.selectedService = name;
    //set the service in local storage
    localStorage.setItem(environment.Service, name);
    this.router.navigate(["/num-verify"]);
    console.log(localStorage.getItem(environment.Service));

  }

  constructor(private router: Router, private categorylist: ServiceSelectionService) {}

  ngOnInit() {
    this.getCategoryList();
  }

  getCategoryList() {
    this.categorylist.getCategoryList().subscribe(
      (response) => {
        console.log(response.data);
        this.categories = response.data;
      },
      error => console.log(error));
  }
}
